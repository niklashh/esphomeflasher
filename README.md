# ESPHome-Flasher

ESPHome-Flasher is a utility app for the [ESPHome](https://esphome.io/)
framework and is designed to make flashing ESPs with ESPHome as simple as possible by:

 * Having pre-built binaries for most operating systems.
 * Hiding all non-essential options for flashing. All necessary options for flashing
   (bootloader, flash mode) are automatically extracted from the binary.

This project was originally intended to be a simple command-line tool,
but then I decided that a GUI would be nice. As I don't like writing graphical
front end code, the GUI largely is based on the
[NodeMCU PyFlasher](https://github.com/marcelstoer/nodemcu-pyflasher)
project.

The flashing process is done using the [esptool](https://github.com/espressif/esptool)
library by espressif.

Install `requests` (required)

```shell
pip install --user requests
```

Clone the project and run the flasher

```shell
git clone https://gitlab.com/niklashh/esphomeflasher
PYTHONPATH=$PWD python esphomeflasher -p /dev/tty<device> <firmware.bin>
```

## Help

```
usage: esphomeflasher
  [-h]
  [-p PORT]
  [--esp8266 | --esp32 | --upload-baud-rate UPLOAD_BAUD_RATE]
  [--bootloader BOOTLOADER]
  [--partitions PARTITIONS]
  [--otadata OTADATA]
  [--no-erase]
  [--show-logs]
  binary

positional arguments:
  binary                The binary image to flash. Required

optional arguments:
  -h, --help            show this help message and exit
  -p PORT, --port PORT  Select the USB/COM port for uploading.
  --esp8266
  --esp32
  --upload-baud-rate UPLOAD_BAUD_RATE
  Baud rate to upload with (not for logging)
  --bootloader BOOTLOADER
  (ESP32-only) The bootloader to flash.
  --partitions PARTITIONS
  (ESP32-only) The partitions to flash.
  --otadata OTADATA     (ESP32-only) The otadata file to flash.
  --no-erase            Do not erase flash before flashing
  --show-logs           Only show logs
```

## License

[MIT](http://opensource.org/licenses/MIT) © Marcel Stör, Otto Winter
